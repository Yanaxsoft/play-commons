package ch.insign.commons.controllers

import javax.inject.{Inject, Singleton}

import controllers.Assets.Asset
import controllers.AssetsBuilder
import play.api.http.{HttpErrorHandler, LazyHttpErrorHandler}

/**
  * @author Urs Honegger &lt;u.honegger@insign.ch&gt;
  */
@Singleton
class Assets @Inject() (errorHandler: HttpErrorHandler) extends controllers.Assets(errorHandler) {
  def lib(path: String, file: Asset) = versioned(path, file)
}

object Assets extends AssetsBuilder(LazyHttpErrorHandler) {
}
