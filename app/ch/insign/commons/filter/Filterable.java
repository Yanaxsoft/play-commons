package ch.insign.commons.filter;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

/**
 * Marks a class as applying content filters through FilterManager.
 *
 * Note: the implementing class is solely responsible of the way it treats filters,
 * e.g. only on input or also on output (output is usually not done in beans but
 * by controllers, to distinguish between admin editing and frontend display).
 *
 * @author bachi
 */
public interface Filterable {

    public void filterInputData();
    public void filterOutputData();
}
