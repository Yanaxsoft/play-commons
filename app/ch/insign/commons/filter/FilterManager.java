package ch.insign.commons.filter;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import play.twirl.api.Html;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Manages the filtering of cms content.
 * @author bachi
 *
 */
public class FilterManager  {
	private final static Logger logger = LoggerFactory.getLogger(FilterManager.class);

    protected List<ContentFilter> filterChain = new ArrayList<>();
    protected HashMap<String, ContentFilter> tagFilters = new HashMap<>();

    enum EventType {
        INPUT, OUTPUT
    }

    public void register(ContentFilter... filters) {

        for (int i = 0; i < filters.length; i++) {
            ContentFilter filter = filters[i];

            // Register the filter (ordered)
            filter.setFilterManager(this);
            filterChain.add(filter);
            logger.debug("Added content filter: " + filter);

            // Create a tag filter lookup map
            if (filter.filterTags() != null) {
                List<String> tags = Arrays.asList(filter.filterTags());
                for (String tag : tags) {
                    if (tagFilters.containsKey(tag)) {
                        // Note: Could allow multiple filters for one tag, but for tags that would be a far-fetched use-case.
                        logger.warn("Tag [[" + tag + "]]: Filter " + filter + " overrides previous filter " + tagFilters.get(tag));
                    }
                    logger.debug("Tag [[" + tag + "]] handled by " + filter);
                    tagFilters.put(tag, filter);
                }
            }
        }
    }

    public void flush() {
        filterChain.clear();
    }

    public List<ContentFilter> getFilterChain() {
        return filterChain;
    }

    public HashMap<String, ContentFilter> getTagFilters() {
        return tagFilters;
    }

//    public void processInput(MString input) {
//        for (String lang : Language.getAllLanguages()) {
//            input.set(lang, processInput(input.get(lang)));
//        }
//    }

    public String processInput(String input, Filterable source) {
        String result;
        for (ContentFilter filter : filterChain) {
            result = filter.processInput(input, source);
            if (result != null) {
                input = result;
            }
        }
        return input;
    }

//    public void processOutput(MString output) {
//        for (String lang : Language.getAllLanguages()) {
//            output.set(lang, processOutput(output.get(lang)));
//        }
//    }

    public String processOutput(String output, Filterable source) {
        String result;
        for (ContentFilter filter : filterChain) {
            result = filter.processOutput(output, source);
            if (result != null) {
                output = result;
                //logger.debug("Applied filter " + filter + " to output.");
            }
        }
        return output;
    }

    public Html processOutput(Html output, Filterable source) {
        return Html.apply(processOutput(output.toString(), source));
    }


    /**
     * @return a String result of applying registered filter to the tag or null
     *         if tag was not processed or didn't match any registered filter.
     */
    @Nullable
    public String processTag(String tag, EventType type, Filterable source) {
        List<String> params = Arrays.asList(tag.split(":"));
        String tagName = params.get(0);

        if ( ! tagFilters.containsKey(tagName)) {
            logger.warn("No filter found for: [[" + tag + "]]");
            return null;
        }

        String result = null;
        ContentFilter filter = tagFilters.get(tagName);
        switch (type) {
            case INPUT:
                result = filter.processTagInput(tag, params, source);
                break;
            case OUTPUT:
                result = filter.processTagOutput(tag, params, source);
                break;
        }

        logger.debug("Applied filter " + filter + " to tag on " + type.toString().toLowerCase()
                + ": [[" + tag + "]] -> " + result);
        return result;
    }

}
