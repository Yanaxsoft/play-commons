package ch.insign.commons.date;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by uhon on 07.07.14.
 */
public class DateHelper {
	/**
	 * Convert a java.util.date into ISO 8601 string format.
	 *
	 * @param date
	 * @return ISO 8601 formatted date: yyyy-MM-dd'T'HH:mm
	 */
	public static String DateToISO(Date date) {
		if (date == null) {
			return "";
		}

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
		return df.format(date);
	}

	/**
	 * Convert a java.util.date to swiss date Format
	 *
	 * @param date
	 * @return formatted date: dd.MM.yyyy
	 */
	public static String dateFormatSwiss(Date date) {
		if (date == null) {
			return "";
		}
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");

		return simpleDateFormat.format(date);
	}

	/**
	 * Convert a java.util.date to swiss date Format including Time
	 *
	 * @param date
	 * @return formatted date: dd.MM.yyyy - HH:mm:ss
	 */
	public static String dateTimeFormatSwiss(Date date) {
		if (date == null) {
			return "";
		}
		SimpleDateFormat simpleDateTimeFormat = new SimpleDateFormat("dd.MM.yyyy - HH:mm:ss");

		return simpleDateTimeFormat.format(date);
	}

}
