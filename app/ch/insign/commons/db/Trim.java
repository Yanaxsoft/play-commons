package ch.insign.commons.db;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

/**
 * @author Timo Schmid <timo.schmid@gmail.com>
 */
@java.lang.annotation.Target({java.lang.annotation.ElementType.FIELD})
@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
public @interface Trim {
    /**
     * TODO could be improved on by left and right trim.
     */
    // boolean left() default true;
    // boolean right() default true;
}