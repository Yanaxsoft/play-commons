package ch.insign.commons.db;

import com.google.common.collect.AbstractIterator;

import java.util.Iterator;

/**
 * As long as JPA can't be used to iterate over large result set,
 * ChunkIterator can be used to iterate large amount of data by reading it from DB chunk by chunk.
 *
 * @param <T> Entity class
 */
public abstract class ChunkIterator<T> extends AbstractIterator<T> implements Iterable<T> {
    private static long CHUNK_SIZE = 400;

    private Iterator<T> chunk;
    private Long count;
    private long index = 0;
    private long chunkSize;

    /**
     * @param count Elements count
     */
    public ChunkIterator(Long count) {
        this(count, CHUNK_SIZE);
    }

    public ChunkIterator(Long count, long chunkSize) {
        super();
        this.count = count;
        this.chunkSize = chunkSize;
    }

    public abstract Iterator<T> getChunk(long index, long chunkSize);

    @Override
    public Iterator<T> iterator() {
        return this;
    }

    @Override
    protected T computeNext() {
        if (count == 0) return endOfData();
        if (chunk != null && chunk.hasNext() == false && index >= count)
            return endOfData();
        if (chunk == null || chunk.hasNext() == false) {
            chunk = getChunk(index, chunkSize);
            index += chunkSize;
        }
        if (chunk == null || chunk.hasNext() == false)
            return endOfData();
        return chunk.next();
    }

}
