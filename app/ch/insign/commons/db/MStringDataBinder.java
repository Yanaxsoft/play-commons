package ch.insign.commons.db;

import ch.insign.commons.i18n.Language;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import play.data.validation.ValidationError;
import play.i18n.Lang;
import play.i18n.Messages;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Timo Schmid <timo.schmid@gmail.com>
 */
public class MStringDataBinder implements DataBinding.DataBinder  {
	private final static Logger logger = LoggerFactory.getLogger(MStringDataBinder.class);

    @Override
    public Object getFromData(String fieldName, Object object, Map<String, String> data) {
        MString mstring;
        if(object == null) {
            mstring = new MString();
        } else {
            mstring = (MString)object;
        }
        for(Lang lang : Lang.availables()) {
            mstring.set(lang.code(), data.get(fieldName + "." + lang.code()));
        }
        return mstring;
    }

    @Override
    public Map<String,String> toData(String fieldName, Object mstring) {
        Map<String,String> data = new HashMap<>();
        if(mstring == null) {
            return data;
        }
        for(Lang lang : Lang.availables()) {
            data.put(fieldName + "." + lang.code(), ((MString)mstring).get(lang.code()));
        }
        return data;
    }

    @Override
    public Map<String,List<ValidationError>> validate(String fieldName, Object object, Object formEntityObject, Map<String, String> formData) {
        MString mstringObj = (MString)object;
        Map<String,List<ValidationError>> errors = new HashMap<>();

        java.lang.reflect.Field field = getInheritedField(formEntityObject.getClass(), fieldName);
        if (field == null) {
            // no such field name in form object
            return new HashMap<>();
        }

        // check for known constraint annotation
        for(Annotation annotation : field.getDeclaredAnnotations()) {
            if (annotation instanceof MString.MStringRequired) {
                String message = ((MString.MStringRequired) annotation).message();
                for(String lang: Language.getAllLanguages()) {
                    if (mstringObj.get(lang).trim().length() == 0) {
                        List<ValidationError> validationErrors = new ArrayList<>();
                       // annotation.
                        validationErrors.add(new ValidationError("errors.required", Messages.get(message)));
                        errors.put(fieldName + "." + lang, validationErrors);
                    }
                }
            }

            if (annotation instanceof MString.MStringRequiredForVisibleTab ) {
                String message = ((MString.MStringRequiredForVisibleTab) annotation).message();
                for(String lang: Language.getAllLanguages()) {
                    boolean isVisible = formData.containsKey("visible." + lang);;
                    if (isVisible && (mstringObj.get(lang).trim().length() == 0) ) {
                        List<ValidationError> validationErrors = new ArrayList<>();
                        validationErrors.add(new ValidationError("errors.required.visible", Messages.get(message)));
                        errors.put(fieldName + "." + lang, validationErrors);
                    }
                }
            }
        }

        return errors;
    }


    /**
     * Access to private inherited fields via reflection
     * http://stackoverflow.com/questions/3567372/access-to-private-inherited-fields-via-reflection-in-java
     *
     * @param type
     * @param fieldName
     * @return
     */
    private Field getInheritedField(Class<?> type, String fieldName) {
        Class<?> i = type;
        while (i != null && i != Object.class) {
            try {
                Field field = i.getDeclaredField(fieldName);
                return field;
            } catch (NoSuchFieldException e) { }

            i = i.getSuperclass();
        }

        return null;
    }

}
