package ch.insign.commons.db;

import com.uaihebert.factory.EasyCriteriaFactory;
import com.uaihebert.model.EasyCriteria;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.data.validation.ValidationError;
import play.i18n.Messages;
import play.db.jpa.JPA;

import javax.persistence.*;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Base Class for JPA Models Provides Finder functionality like the Ebean model
 * or the old JPA Model.
 *
 * @author bachi
 * @author Timo Schmid <t.schmid@insign.ch>
 */
@MappedSuperclass
public abstract class Model  {
	private final static Logger logger = LoggerFactory.getLogger(Model.class);
	// Introduced to debug JPA Caching or Leaks, will log to jpa.log
	protected final static Logger jpaLogger = LoggerFactory.getLogger(Model.class);

    /**
     * This exception is thrown if a JPA persisted entity could not be merged
     * with a form's data. This most likely indicates wrong use of the
     * form/entity API.
     */
    public static class UpdateFromFormException extends RuntimeException  {
        public UpdateFromFormException() {
            super();
        }
        public UpdateFromFormException(String message) {
            super(message);
        }
        public UpdateFromFormException(String message, Throwable cause) {
            super(message, cause);
        }
        public UpdateFromFormException(Throwable cause) {
            super(cause);
        }
        private static final long serialVersionUID = 1L;
    }


    @Transient
    @Deprecated
    private List<Field> fieldList;

    public static final int PAGINATE_DEFAULT = 100;

    @Id
    @GeneratedValue
    private long id;

    /**
     * Get the string representation of the id.
     *
     * @return
     */
    public String getId() {
        // FIXME: ensure non-valid ids are returned as null, not 0 or smth
        // if (id == 0) return null; // no id = null. OR: use Long not long for
        // the id.
        if (id == 0) return null;
        return String.valueOf(id);
    }

    /**
     * Get the id in its raw format, as used e.g. for JPA.em().find()
     * INTERNAL use only - use getId(). Since we might switch to string-based UUIDs later
     *
     * @return
     */
    public long getRawId() {
        return id;
    }

    /**
     * Set the id from its string representation.
     *
     * @param id
     */
    public void setId(String id) {
        this.id = Long.valueOf(id);
    }

    /**
     * Saves (inserts new or updates existing) entity. Further changes are
     * stored automatically once the entity is persisted.
     */
    public void save() {
        if (JPA.em().contains(this)) {
            logger.info("Updating existing " + this);
            JPA.em().merge(this);
        } else {
            JPA.em().persist(this);
            logger.info("Persisted " + this);
        }
    }

    /**
     * Deletes (removes) this entity.
     */
    public void delete() {
        logger.info("Deleting " + this);
        JPA.em().remove(this);
    }

    /**
     * Refreshes this entity from the database.
     */
    public void refresh() {
        if (JPA.em().contains(this)) {
            JPA.em().refresh(this);
        }
    }

    /**
     * <b>Deprecated: SmartForm can do everything now!</b>
     *
     * @param boundForm
     * @throws UpdateFromFormException
     */
//    @Deprecated
//    public void updateFromForm(Form<? extends Model> boundForm) throws UpdateFromFormException {
//
//        Model detachedEntity = boundForm.get();
//
//        // Loop through all form fields and copy the property value from the
//        // detached to
//        // the persisted instance for each submitted form field.
//        // Assumption: The form field name matches exactly the property name (as
//        // assumes play's form class)
//        for (String property : boundForm.data().keySet()) {
//
//            logger.trace("UpdateFromForm: Trying to merge form field: '" + property + "'");
//
//            // Get the field
//            try {
//                Field field = getFieldByName(property);
//
//                boolean accessible = field.isAccessible();
//
//                field.setAccessible(true);
//
//                // Read the field
//                Object value = null;
//                try {
//                    value = field.get(detachedEntity);
//                } catch (IllegalArgumentException | IllegalAccessException e) {
//                    field.setAccessible(accessible);
//                    throw new UpdateFromFormException("Could not read field with name '" + property + "'", e);
//                }
//
//                // Set the field if its value is not null
//                if (value != null) {
//
//                    // 1st try to invoke the setter
//                    try {
//                        Method setter = new PropertyDescriptor(property, this.getClass()).getWriteMethod();
//                        setter.invoke(this, value);
//
//                        logger.debug("UpdateFromForm: Updated " + property + " (using setter)");
//
//                    } catch (Exception e) {
//
//                        // 2nd if the setter is not available, try to set the
//                        // property value directly
//                        try {
//                            field.set(this, value);
//                            logger.debug("UpdateFromForm: Updated " + property + " (directly, no setter)");
//                        } catch (IllegalArgumentException | IllegalAccessException e1) {
//                            field.setAccessible(accessible);
//                            throw new UpdateFromFormException("Could not set field with name '" + property + "'", e1);
//                        }
//                    }
//                }
//
//            } catch (NoSuchFieldException e1) {
//                logger.warn("UpdateFromForm: Form field '" + property + "' not found in entity " + this);
//                continue;
//            } catch (SecurityException e2) {
//                throw new UpdateFromFormException("Could not access field with name '" + property + "'", e2);
//            }
//        }
//    }

    /**
     * "Always override hashCode when you override equals" [1][2].
     *
     * 1. Bloch, Joshua (2008), Effective Java (2 ed.)
     * 2. http://en.wikipedia.org/wiki/Java_hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (id ^ (id >>> 32));
        return result;
    }

    /**
     * Compare two entity objects, following hibernate semantics for equality.
     * Here we assume that new objects are always different unless they are the
     * same object. If an object is loaded from the database it has a valid id
     * and therefore we can check against object ids.
     *
     * Subclasses that have anotion of logical equality shall override equals.
     * This is generally the case for value classes, such as Integer or Date [1].
     *
     * 1. Bloch, Joshua (2008), Effective Java (2 ed.), Chapter 3
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null) {
            return false;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        Model other = (Model) obj;
        if (id != other.id) {
            return false;
        }

        if (getId() == null || other.getId() == null) {
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " (" + this.getId() + ")";
    }

    /**
     * Finder class. Helps to do everyday business, contains methods to find
     * objects from the database.
     *
     * @param <Entity>
     *            the entity to find.
     */
    public static class Finder<Entity extends Model> {

        private final Class<Entity> entityClass;

        public Finder(Class<Entity> entityClass) {
            this.entityClass = entityClass;
        }

        public Entity byId(final String id) {
            if (id == null) {
                logger.warn("Expected String id, but null given in "
                        + this.getClass().getSimpleName()
                        + ".byId(String id)");
                return null;
            }

            try {
                Long.parseLong(id);
            } catch (NumberFormatException e){
                return null;
            }

            return JPA.em().find(Finder.this.entityClass, Long.parseLong(id));
        }

        public EasyCriteria<Entity> query() {
            return EasyCriteriaFactory.createQueryCriteria(JPA.em(), Finder.this.entityClass);
        }

        public List<Entity> all() {
            return query().getResultList();
        }

        public long count() {
            return query().count();
        }

        protected Class<Entity> getEntityClass() {
            return this.entityClass;
        }

        public Paginate<Entity> getPaginate() {
            return getPaginate(query(), PAGINATE_DEFAULT);
        }

        public Paginate<Entity> getPaginate(int itemsPerPage) {
            return getPaginate(query(), itemsPerPage);
        }

        public Paginate<Entity> getPaginate(EasyCriteria<Entity> query) {
            return getPaginate(query, PAGINATE_DEFAULT);
        }

        public Paginate<Entity> getPaginate(EasyCriteria<Entity> query, int itemsPerPage) {
            return new Paginate<Entity>(query, itemsPerPage);
        }

    }

    /**
     * Get a (cached) list of all fields of this object and all superclasses.
     *
     * @return
     */
    protected List<Field> getAllFields() {
        if (fieldList == null) {
            fieldList = new ArrayList<Field>();
            Class<?> i = this.getClass();
            while (i != null && i != Object.class) {
                fieldList.addAll(Arrays.asList(i.getDeclaredFields()));
                i = i.getSuperclass();
            }
        }
        return fieldList;
    }

    /**
     * Get the Field by its name, regardless where in the class hierarchy it was
     * defined.
     *
     * @param name
     * @return Field
     * @throws NoSuchFieldException
     */
    protected Field getFieldByName(String name) throws NoSuchFieldException {
        for (Field f : getAllFields()) {
            if (f != null && f.getName().equals(name)) {
                return f;
            }
        }
        throw new NoSuchFieldException();
    }



    // Delegate JPA events to the ModelEvent class

    @PrePersist
    private void onPrePersist() {
        ModelEvents.onPrePersist(this);
    }

    @PreUpdate
    private void onPreUpdate() {
        ModelEvents.onPreUpdate(this);
    }

    @PreRemove
    private void onPreRemove() {
        ModelEvents.onPreRemove(this);
    }

    @PostPersist
    private void onPostPersist() {
        ModelEvents.onPostPersist(this);
    }

    @PostUpdate
    private void onPostUpdate() {
        ModelEvents.onPostUpdate(this);
    }

    @PostRemove
    private void onPostRemove() {
        ModelEvents.onPostRemove(this);
    }

    protected static boolean checkEquals(Model o1, Model o2){
		if (o1 == null)
			return o2 == null;
		if (o2 == null)
			return false;
		else
			return o1.getId().equals(o2.getId());
	}

    /**
     * Creates a list of validation errors.
     * @param fieldName The field name for this error message
     * @param errorMessageKey The error message key for this field
     * @return A list containing the field errors.
     */
    protected List<ValidationError> addValidationError(String fieldName, String errorMessageKey) {
        return addValidationError(null, fieldName, errorMessageKey);
    }

    /**
     * Adds validation errors to an existing list of validation errors.
     * @param errors The list of errors to add the new error to. Can be null to create a new list.
     * @param fieldName The field name for this error message
     * @param errorMessageKey The error message key for this field
     * @return A list containing the field errors.
     */
    protected List<ValidationError> addValidationError(List<ValidationError> errors, String fieldName, String errorMessageKey) {
        if(errors == null) {
            errors = new ArrayList<>();
        }
        errors.add(new ValidationError(fieldName, Messages.get(errorMessageKey)));
        return errors;
    }
}
