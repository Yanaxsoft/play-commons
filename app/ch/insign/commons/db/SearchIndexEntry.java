package ch.insign.commons.db;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;

/**
 * Deprecated - use ElasticSearch instead.
 *
 * Base Class for JPA Models that can be found with a search.
 * The field "searchContent" contains all the data that can be searched.
 * Implementing classes should define the method "getSearchableContent" to return what content can be searched.
 *
 */
@Deprecated
@Entity
@Table(name = "cmn_search_index", indexes = {
		@Index(name = "search_index_entry_type", columnList = "search_index_entry_type")})
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "search_index_entry_type")
public class SearchIndexEntry extends Model  {
	private final static Logger logger = LoggerFactory.getLogger(SearchIndexEntry.class);

	@Lob
    @Column(nullable = false)
    private String searchContent;

    public String getSearchContent() {
        return this.searchContent;
    }

    public void setSearchContent(String[] searchContent) {
        this.searchContent = StringUtils.join(searchContent, "|||");
    }

    public static Finder<SearchIndexEntry> find = new Finder<>(SearchIndexEntry.class);

}