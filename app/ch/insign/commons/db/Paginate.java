package ch.insign.commons.db;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import com.uaihebert.model.EasyCriteria;

import javax.persistence.TypedQuery;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Helper object for a pagination. All the page-numbers are user-readable, not 0-index based (page 1 is the first).
 * @author Timo Schmid <timo.schmid@gmail.com>
 * @param <T> The Model class
 */
public class Paginate<T extends Model> {
    public static final int PAGINATE_ALL = -1;

    public  static final int DEFAULT_ITEM_PER_PAGE = 50;

    private int totalItemCount;

    private int itemsPerPage;

    private Query<T> query;

    private final HashMap<Integer,Page<T>> pages = new HashMap<>();

    private final List<Integer> paginateItemforSelect = Arrays.asList(20, 50, 100, PAGINATE_ALL);

    private final List<Integer> shortItemsPerPageList = Arrays.asList(20, 50, 100);

    /**
     * Creates a new paginate for EasyCriteria source.
     * @param query The query to get the items.
     * @param itemsPerPage The amoount of items per page.
     */
    public Paginate(final EasyCriteria<T> query, int itemsPerPage) {
	    preparePaginate(query, itemsPerPage, query.count().intValue());
    }

	public Paginate(final EasyCriteria<T> query, int itemsPerPage, int totalItemCount) {
		preparePaginate(query, itemsPerPage, totalItemCount);
	}

	private void preparePaginate(final EasyCriteria<T> query, int itemsPerPage, int totalItemCount) {
		if(itemsPerPage == PAGINATE_ALL) {
			this.itemsPerPage = totalItemCount;
		} else {
            if (shortItemsPerPageList.contains(itemsPerPage)) {
                this.itemsPerPage = itemsPerPage;
            } else {
                this.itemsPerPage = DEFAULT_ITEM_PER_PAGE;
            }
		}

		this.totalItemCount = totalItemCount;
		this.query = new Query<T>() {

			public Query setFirstResult(int firstResult) {
				query.setFirstResult(firstResult);
				return this;
			}

			public Query setMaxResults(int maxResults) {
				query.setMaxResults(maxResults);
				return this;
			}

			public List<T> getResultList() {
				return query.getResultList();
			}
		};

	}



    /**
     * Create a new paginate for TypedQuery source.
     * Hint: Do not use TypedQuery if you can use EasyCriteria.
     */
    @Deprecated
    public Paginate(final TypedQuery<T> query, int itemsPerPage, int totalItemCount) {

        if(itemsPerPage == PAGINATE_ALL) {
            this.itemsPerPage = totalItemCount;
        } else {
            if (shortItemsPerPageList.contains(itemsPerPage)) {
                this.itemsPerPage = itemsPerPage;
            } else {
                this.itemsPerPage = DEFAULT_ITEM_PER_PAGE;
            }
        }

        this.totalItemCount = totalItemCount;

        this.query = new Query<T>() {

            public Query setFirstResult(int firstResult) {
                query.setFirstResult(firstResult);
                return this;
            }

            public Query setMaxResults(int maxResults) {
                query.setMaxResults(maxResults);
                return this;
            }

            public List<T> getResultList() {
                return query.getResultList();
            }
        };
    }

    public List<Integer> getPaginateItemforSelect() {
        return paginateItemforSelect;
    }

    public List<Integer> getShortItemsPerPageList() {
        return shortItemsPerPageList;
    }

    public int getItemsPerPage() {
        return itemsPerPage;
    }

    /**
     * Returns the total amount of items in the list.
     * @return The total amount of items in the list.
     */
    public int getTotalItemCount() {
        return this.totalItemCount;
    }

    /**
     * Returns the amount of pages.
     * @return The amount of pages.
     */
    public int getTotalPageCount() {

	    if(getTotalItemCount() == 0) {
		    return 1;
	    }

        int pages = getTotalItemCount() / this.itemsPerPage;
        if(getTotalItemCount() % this.itemsPerPage > 0) {
            return pages + 1;
        } else {
            return pages;
        }
    }

    /**
     * Returns the page with the specified number.
     * @param pageNumber The page number.
     * @return The Page object for this page.
     */
    public Page<T> getPage(int pageNumber) {
        if(pageNumber < 1) {
            throw new IllegalArgumentException("The parameter itemsPerPage must be 1 or greater.");
        }

        if(pages.containsKey(pageNumber)) {
            return pages.get(pageNumber);
        }

        Page<T> page = new Page<T>(query, pageNumber, getTotalPageCount(), itemsPerPage, getTotalItemCount());
        pages.put(pageNumber, page);
        return page;

    }

    /**
     * Interface used to control query execution.
     * @param <T> query result type
     */
    private static interface Query<T> {
        Query setFirstResult(int firstResult);
        Query setMaxResults(int maxResults);
        List<T> getResultList();
    }

    /**
     * Represents a page in the pagination.
     * @param <T> The Model class
     */
    public static class Page<T> {

        private final int pageNumber;

        private final int totalPageCount;

        private final int firstItem;

        private final int lastItem;

        private final int totalItemCount;

        private final List<T> results;

        /**
         * Creates a new page.
         * @param query The query to get the pages elements
         * @param pageNumber The number of this page
         * @param totalPageCount The total amount of pages
         * @param itemsPerPage The amount of items on a page
         */
        private Page(Query<T> query, int pageNumber, int totalPageCount, int itemsPerPage, int totalItemCount) {
            this.pageNumber = pageNumber;
            this.totalPageCount = totalPageCount;
            this.totalItemCount = totalItemCount;
            this.firstItem = ((pageNumber - 1) * itemsPerPage);
            this.results = query.setFirstResult(firstItem).setMaxResults(itemsPerPage).getResultList();
            this.lastItem = firstItem + results.size();
        }

        public int getPageNumber() {
            return this.pageNumber;
        }

        public int getTotalPageCount() {
            return this.totalPageCount;
        }

        public int getFirstItem(){
            return this.firstItem + 1;
        }

        public int getLastItem() {
            return this.lastItem;
        }

        public int getTotalItemCount() {
            return this.totalItemCount;
        }

        public List<T> getList() {
            return results;
        }

    }

}
