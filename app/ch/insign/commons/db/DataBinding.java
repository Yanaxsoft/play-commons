package ch.insign.commons.db;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import play.data.validation.ValidationError;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Timo Schmid <timo.schmid@gmail.com>
 */

public class DataBinding  {
	private final static Logger logger = LoggerFactory.getLogger(DataBinding.class);

    private DataBinding() {}

    private static Map<Class<?>, DataBinder> registeredDataBinders = new HashMap<>();

    public static boolean isSupported(Class<?> clazz) {
        return registeredDataBinders.containsKey(clazz);
    }

    public static void registerDataBinder(Class<?> clazz, DataBinder dataBinder) {
        if(registeredDataBinders.containsKey(clazz)) {
            logger.info("There already is a DataBinder registered for the class " + clazz.getSimpleName() +". Skipping.");
            return;
        }
        registeredDataBinders.put(clazz, dataBinder);
    }

    public static Map<String,String> getMap(Class<?> clazz, String fieldName, Object o) {
        return getDataBinder(clazz).toData(fieldName, o);
    }

    public static Map<String,List<ValidationError>> validate(String fieldName, Object o, Object entityObject, Map<String, String> formData) {
        return getDataBinder(o.getClass()).validate(fieldName, o, entityObject, formData);
    }

    private static DataBinder getDataBinder(Class<?> clazz) {
        if(isSupported(clazz)) {
            return registeredDataBinders.get(clazz);
        } else {
            throw new IllegalArgumentException("There is no DataBinder registered for the class " + clazz.getSimpleName() + ". Did you forget to register the DataBinder?");
        }
    }

    public static Object bind(Class<?> clazz, String fieldName, Object object, Map<String,String> values) {
        return getDataBinder(clazz).getFromData(fieldName, object, values);
    }


    public interface DataBinder {

        public Object getFromData(String fieldName, Object instance, Map<String,String> data);

        public Map<String,String> toData(String fieldName, Object t);

        public Map<String,List<ValidationError>> validate(String fieldName, Object object, Object formEntityObject, Map<String, String> formData);

    }

}
