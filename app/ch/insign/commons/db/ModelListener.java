package ch.insign.commons.db;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

/**
 * Listener for model JPA events.
 * @author bachi
 */
public class ModelListener<T extends Model> {
    public void onPrePersist(T model) {}
    public void onPreUpdate(T model) {}
    public void onPreRemove(T model) {}
    public void onPostPersist(T model) {}
    public void onPostUpdate(T model) {}
    public void onPostRemove(T model) {}
}
