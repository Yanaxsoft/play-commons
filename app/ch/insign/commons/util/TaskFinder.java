package ch.insign.commons.util;

import play.db.jpa.JPA;
import ch.insign.commons.db.Model;
import com.uaihebert.model.EasyCriteria;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.time.Instant;
import java.util.*;

/**
 * Created by bachi on 20.07.14.
 */
public class TaskFinder<T extends Task> extends Model.Finder<T> {
    private final static Logger logger = LoggerFactory.getLogger(TaskFinder.class);
    public final static int ALL_TASK = -1;
    public final static int OPEN_TASK = 0;
    public final static int FINISHED_TASK = 1;
    public final static int FAILED_TASK = 2;


    public TaskFinder(Class<T> tClass) {
        super(tClass);
    }

    /**
     * Find all tasks of this queue
     */
    public List<T> byQueue(String queueName) {

        EasyCriteria<T> query = query();
        try {
            query = query.andEquals("queueName", queueName);
            return query.getResultList();

        } catch (Exception e) {

            e.printStackTrace();
            logger.error(e.getMessage(), e);
            return new ArrayList<T>();
        }
    }

    /**
     * Find all unfinished tasks in the given queue.
     */
    public List<T> queueRestart(String queueName) {

         EasyCriteria<T> query = query();
        try {
            query = query
                    .andEquals("queueName", queueName)
                    .andEquals("finished", false);

            return query.getResultList();

        } catch (Exception e) {

            e.printStackTrace();
            logger.error(e.getMessage(), e);
            return new ArrayList<T>();
        }
    }

    public  TypedQuery<Task> findExpiredByType(Class taskType, Instant deadline) {
        return JPA.em().createNamedQuery("Task.findExpiredByType", Task.class)
                .setParameter("taskType", taskType.getSimpleName())
                .setParameter("deadline", deadline);
    }

    public  Long findExpiredByTypeCount(Class taskType, Instant deadline) {
        return JPA.em().createNamedQuery("Task.findExpiredByType.count", Long.class)
                .setParameter("taskType", taskType.getSimpleName())
                .setParameter("deadline", deadline).getSingleResult();
    }

    public List<String> findAllTags() {
		return JPA.em().createQuery("SELECT DISTINCT t.tag FROM Task AS t", String.class).getResultList();
    }

	public EasyCriteria<T> easyGetAllBySortingAndFiltersParametrs(String sortBy, String order,String filterByTag, int filterFinished, String reviewStatusString) {
        Task.ReviewStatus reviewStatus = null;
        try {
            reviewStatus = Task.ReviewStatus.valueOf(reviewStatusString);
        } catch (Exception e) {}

		EasyCriteria<T> query = query();
		if (StringUtils.isNotBlank(filterByTag)) {
			query.andEquals("tag", filterByTag);
		}
		if (filterFinished == 1) {
			query.andEquals("finished", true);
		}
        if (filterFinished == 0) {
            query.andEquals("finished", false);
        }

        if (filterFinished == FAILED_TASK) {
            query.andEquals("finished", true);
            query.andEquals("success", false);
        }

        if (reviewStatus != null) {
            query.andEquals("reviewStatus", reviewStatus);
        }

        if (StringUtils.isBlank(sortBy)) {
            query.orderByDesc("created").orderByDesc("finished");
        } else {
            if (order.equals("asc")) {
                query.orderByDesc(sortBy);
            } else {
                query.orderByAsc(sortBy);
            }
        }
		return query;

	}

    public List<T> easyGetListAllBySortingAndFiltersParametrs(String sortBy, String order,String filterByTag, int filterFinished, String reviewStatus) {
        return easyGetAllBySortingAndFiltersParametrs(sortBy, order, filterByTag, filterFinished, reviewStatus).getResultList();
    }


    /**
     * Find all by sorting and filtering parameters
     */
    public TypedQuery<T> getAllBySortingAndFiltersParametrs(String sortBy, String order,String filterByTag, int filterFinished) {

        CriteriaBuilder builder = JPA.em().getCriteriaBuilder();
        CriteriaQuery<T> query = builder.createQuery(getEntityClass());
        Root<T> tasks = query.from(getEntityClass());

        List<Predicate> conditions = new ArrayList<>();
        Predicate tagPredicate, finishedPredicate;

        if(filterByTag != null && !filterByTag.isEmpty()) {
            tagPredicate = builder.like(
                    builder.lower(tasks.get("tag")),
                    "%" + filterByTag.toLowerCase() + "%");
            conditions.add(tagPredicate);
        }

        if(filterFinished == 0 || filterFinished == 1) {
            finishedPredicate = builder.equal(tasks.get("finished"), filterFinished);
            conditions.add(finishedPredicate);
        }
        @SuppressWarnings("rawtypes")
        Expression orderByFieldExpression;

        if (sortBy.isEmpty() && order.isEmpty()) {
            return JPA.em().createQuery(query
                            .select(tasks)
                            .where(
                                    builder.and(
                                            conditions.toArray(new Predicate[]{})
                                    )
                            )
                            .orderBy(builder.asc(tasks.get("finished")), builder.desc(tasks.get("created")))
            );

        }

        orderByFieldExpression = tasks.get(sortBy);
        return JPA.em().createQuery(query
                        .select(tasks)
                        .where(
                                builder.and(
                                        conditions.toArray(new Predicate[]{})
                                )
                        )
                        .orderBy(order.toLowerCase().equals("desc")
                                ? builder.desc(orderByFieldExpression)
                                : builder.asc(orderByFieldExpression))
        );
    }

}
