package ch.insign.commons.util;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import play.mvc.Controller;
import play.Play;

import java.util.Arrays;
import java.util.List;


/**
 * Manages play config values (from application.conf) in a more safe and easy-to-use way.
 *
 * Settings are configured in the application.conf file using the given config namespace, eg. "mymodule." prefix.
 * Upon instantiation, the test() method is called which should check (call) all defined values to ensure completeness
 * and to prevent later runtime errors.
 */
public abstract class Configuration  {
	private final static Logger logger = LoggerFactory.getLogger(Configuration.class);

    /** internal playConfig holder - always access through getConfig() **/
    private Config playConfig = null;

    /**
     * Constructor - checks for missing playConfig file values on instantiation, as they're
     * inherently a source of runtime errors.
     */
    public  Configuration() {

        try {
            playConfig = ConfigFactory.load().getConfig(configNamespace());
        } catch (Exception e) {
            logger.warn("No config values found for " + configNamespace());
        }

        try {
            test();

        } catch (Exception e) {
            throw new RuntimeException("Missing '" + configNamespace() + "' configuration values in application.conf: " + e.getMessage(), e);
        }
    }

    protected abstract String configNamespace();

    /**
     * Called on instantiation. Add a call to each of your config getter methods in here
     * to ensure they're available.
     */
    protected abstract void test();

    /**
     * Get the cms playConfig object (that is, config values that start with configNamespace(), e.g. "myModule.").
     * E.g .a config value of "myModule.enjoyLife=true" would be accessed like this:
     * getConfig().getBoolean("enjoyLife");
     *
     * Do not access application.conf settings from outside directly, provide a getter for each setting.
     *
     * @return a Typesafe Config object for the "cms." settings.
     */
    protected Config getConfig() {
        return playConfig;
    }

    /**
     * Retrieve base URL or fallbackUrl if current host cannot be determined
     */
    public static String resolveBaseUrl(String fallbackUrl) {
        String baseUrl;
        try {
            baseUrl = "http://" + Controller.request().host();
        } catch (RuntimeException e) {
            baseUrl = Play.application().configuration().getString("application.baseUrl");
        }
        return baseUrl != null ? baseUrl : fallbackUrl;
    }

    // Some static helpers for directly getting config values with defaults if not set

    public static String getOrElse(String key, String defaultValue) {
        if (ConfigFactory.load().hasPath(key)) {
            return ConfigFactory.load().getString(key);
        } else {
            return defaultValue;
        }
    }

    public static boolean getOrElse(String key, boolean defaultValue) {
        if (ConfigFactory.load().hasPath(key)) {
            return ConfigFactory.load().getBoolean(key);
        } else {
            return defaultValue;
        }
    }

    public static int getOrElse(String key, int defaultValue) {
        if (ConfigFactory.load().hasPath(key)) {
            return ConfigFactory.load().getInt(key);
        } else {
            return defaultValue;
        }
    }

    public static List<String> getOrElse(String key, String... defaultValue) {
        if (ConfigFactory.load().hasPath(key)) {
            return ConfigFactory.load().getStringList(key);
        } else {
            return Arrays.asList(defaultValue);
        }
    }
}
