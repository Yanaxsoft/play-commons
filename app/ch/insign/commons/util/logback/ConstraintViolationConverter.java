package ch.insign.commons.util.logback;

import ch.insign.commons.util.ConstraintViolationFormat;
import ch.qos.logback.classic.pattern.ThrowableHandlingConverter;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.classic.spi.ThrowableProxy;
import ch.qos.logback.core.CoreConstants;

import java.util.Optional;

/**
 * See http://logback.qos.ch/manual/layouts.html#customConversionSpecifier
 *
 * Example usage:
 *
 *  <conversionRule conversionWord="cve" converterClass="util.logback.CveConverter" />
 *
 */
public class ConstraintViolationConverter extends ThrowableHandlingConverter {
	@Override
	public String convert(ILoggingEvent event) {
		ThrowableProxy proxy = (ThrowableProxy) event.getThrowableProxy();
		return Optional.ofNullable(proxy)
				.flatMap(p -> ConstraintViolationFormat.formatIfConstraintViolationException(p.getThrowable()))
				.map(str -> CoreConstants.LINE_SEPARATOR + str + CoreConstants.LINE_SEPARATOR)
				.orElse(CoreConstants.EMPTY_STRING);
	}
}
