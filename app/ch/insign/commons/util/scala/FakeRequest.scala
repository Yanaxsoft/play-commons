package ch.insign.commons.util.scala

import java.security.cert.X509Certificate

import play.api.mvc._
import play.api.mvc.Call

/**
 * Fake HTTP headers implementation.
 *
 * @param headers Headers data.
 */
case class FakeHeaders(override val headers: Seq[(String, String)] = Seq.empty) extends Headers(headers: Seq[(String, String)])

/**
 * Fake HTTP request implementation.
 *
 * @tparam A The body type.
 * @param method The request HTTP method.
 * @param uri The request uri.
 * @param headers The request HTTP headers.
 */
case class FakeRequest[A](method: String, uri: String, headers: FakeHeaders, body: A, remoteAddress: String = "127.0.0.1", version: String = "HTTP/1.1", id: Long = 666, tags: Map[String, String] = Map.empty[String, String], secure: Boolean = false) extends Request[A] {

  private def _copy[B](
                        id: Long = this.id,
                        tags: Map[String, String] = this.tags,
                        uri: String = this.uri,
                        path: String = this.path,
                        method: String = this.method,
                        version: String = this.version,
                        headers: FakeHeaders = this.headers,
                        body: B,
                        remoteAddress: String = this.remoteAddress,
                        secure: Boolean = this.secure): FakeRequest[B] = {
    new FakeRequest[B](
      method, uri, headers, body, remoteAddress, version, id, tags, secure
    )
  }

  /**
   * The request path.
   */
  lazy val path = uri.split('?').take(1).mkString

  /**
   * The request query String
   */
  lazy val queryString: Map[String, Seq[String]] =
    play.core.parsers.FormUrlEncodedParser.parse(rawQueryString)

  override def clientCertificateChain: Option[Seq[X509Certificate]] = Option.empty

}

/**
 * Helper utilities to build FakeRequest values.
 */
object FakeRequest {

  /**
   * Constructs a new GET / fake request.
   */
  def apply(): FakeRequest[AnyContentAsEmpty.type] = {
    FakeRequest("GET", "/", FakeHeaders(), AnyContentAsEmpty)
  }

  /**
   * Constructs a new request.
   */
  def apply(method: String, path: String): FakeRequest[AnyContentAsEmpty.type] = {
    FakeRequest(method, path, FakeHeaders(), AnyContentAsEmpty)
  }

  def apply(call: Call): FakeRequest[AnyContentAsEmpty.type] = {
    apply(call.method, call.url)
  }
}
