package ch.insign.commons.util;

import play.db.jpa.JPA;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ValidationException;
import java.time.Instant;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.stream.Stream;

/**
 * A simple task queue implementation.
 * Note: It relies on external 'ticks' (calls to process()), e.g. from an Akka actor.
 *
 * @author bachi
 */
public class TaskQueue {
	private final static Logger logger = LoggerFactory.getLogger(TaskQueue.class);

	private static TaskQueue instance;
	private static long counter;
	private boolean running = false;
	private final Queue<Task> queue = new PriorityQueue<>();
	private String name;

	public TaskQueue(String name) {
		this.name = name;
	}

	public TaskQueue start() {
		running = true;
		return this;
	}

	public TaskQueue stop() {
		running = false;
		return this;
	}

	/**
	 * Add a task to the queue. The task will be validated before adding.
	 *
	 * @param task the task to add
	 * @return true if the task could be added, false otherwise
	 */
	public synchronized boolean add(Task task) {

		try {
			// Validate the task. If it fails, don't add the task and log as error.
			task.validate();

			task.setCreated(Instant.now());
			task.setQueueName(TaskQueue.this.name);
			counter++;
			task.queueId = counter;
			task.save();
			logger.info("Queueing " + task);

			synchronized (queue) {
				queue.add(task);
			}

			task.onEnqueued();
		}
		catch (ValidationException ev) {
			logger.error("Task validation failed, task was not enqueued: " + task, ev);
			return false;
		}
		catch (Exception e) {
			logger.error("Could not add task to queue: " + task, e);
			return false;
		}

		return true;
	}

	/**
	 * Remove a task from the queue and database.
	 * The task should be a new and should never been executed earlier
	 *
	 * @param task the task to remove
	 */
	public synchronized void remove(Task task) {
		if (task.getReviewStatus() == Task.ReviewStatus.NEW) {
			synchronized (queue) {
				queue.remove(task);
			}
			task.delete();
		}
	}

	public Queue<Task> getQueue() {
		return queue;
	}

	/**
	 * Trigger one execution (e.g. from Akka)
	 */
	public TaskQueue process() {

		if (!running) return this;

		JPA.withTransaction(() -> {

			Task task;
			synchronized (queue) {
				task = queue.peek();
			}
			if (task == null) {
				//logger.debug("Queue is empty");
				return;
			}

			// don't execute if task was manually executed and finished
			if (task.isFinished()) {
				return;
			}

			// If the head of this priority queue is scheduled for later retry, then all others are too
			if (task.getScheduleAfter() != null && task.getScheduleAfter().isAfter(Instant.now())) {
				return;
			}

			synchronized (queue) {
				queue.remove(task);
				//queue.poll();
			}

			Instant now = Instant.now();
			task.setLastTry(now);
			if (task.getTries() == 0) task.setFirstTry(now);
			task.setTries(task.getTries()+1);

			boolean result = false;
			try {

				// Execute the task in its own JPA transaction, so that any rollbacks wont affect the queue code
				result = JPA.withTransaction(task::execute);

			} catch (Exception e) {
				logger.error(name + ": Error while trying to execute " + task, e);
				e.printStackTrace();
			}


			try {

				if (result) {

					logger.info(name + ": Successfully executed " + task);
					task.setFinished(true);
					task.setSuccess(true);
					task.setScheduleAfter(null);

				} else {

					if (task.retry()) {
						logger.warn(name + ": Execution failed, re-queueing " + task);
						task.onRequeue();
						synchronized (queue){
							queue.add(task);
						}

					} else {
						logger.error(name + ": Execution failed permanently, giving up on " + task);
						task.setFinished(true);
						task.setScheduleAfter(null);
						task.failed();
					}
				}

			} catch (Exception e) {
				logger.error(name + ": Error while handling execution result of " + task, e);
				e.printStackTrace();
			}

			JPA.em().merge(task);
		});

		return this;
	}

	public int size() {
		return queue.size();
	}

	/**
	 * Load and re-queue all presisted tasks for this queue.
	 */
	public synchronized TaskQueue load() {

		JPA.withTransaction((Runnable) () -> queue.addAll(Task.find.queueRestart(name)));

		logger.info("Loaded task queue '" + name + "' - tasks found: " + queue.size());
		return TaskQueue.this;
	}

	public Stream<Task> filterQueue(FilterInterface predicate) {
		return queue.stream().filter(t -> predicate.filter(t));
	}

	public boolean existsInQueue(FilterInterface predicate) {
		return queue.stream().anyMatch(t -> predicate.filter(t));
	}

	@FunctionalInterface
	public interface FilterInterface {

		public boolean filter(Task t);

	}

}
