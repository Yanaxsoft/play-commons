package ch.insign.commons.varnish;

import play.mvc.Call;
import play.twirl.api.Html;

/**
 * @author Timo Schmid <timo.schmid@gmail.com>
 */
public interface VarnishCachable {

    Call getCall();

    Html render();

}