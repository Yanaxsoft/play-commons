package ch.insign.commons.varnish;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import play.Play;
import play.libs.ws.WS;

/**
 * @author Timo Schmid <timo.schmid@gmail.com>
 */
public class Varnish  {
	private final static Logger logger = LoggerFactory.getLogger(Varnish.class);

    public static final String CONFIG_KEY_ENABLE = "varnish.enable";

    public static final String CONFIG_KEY_HOSTNAME = "varnish.hostname";

    public static final String CONFIG_DEFAULT_HOSTNAME = "127.0.0.1";

    public static final String CONFIG_KEY_PORT = "varnish.port";

    public static final int CONFIG_DEFAULT_PORT = 6081;

    public static void remove(VarnishCachable cacheObj) {
        if(Play.application().configuration().getBoolean(CONFIG_KEY_ENABLE, false)) {
            String hostname = Play.application().configuration().getString(CONFIG_KEY_HOSTNAME, CONFIG_DEFAULT_HOSTNAME);
            int port = Play.application().configuration().getInt(CONFIG_KEY_PORT, CONFIG_DEFAULT_PORT);
            logger.debug("Removing url from cache: " + cacheObj.getCall().url());
            String varnishUrl = "http://" + hostname + ":" + port + "/" + cacheObj.getCall().url();
            logger.debug("Call to varnish: " + varnishUrl);
            WS.url(varnishUrl).execute("PURGE");
            // TODO Check if that works
        }
    }

}
