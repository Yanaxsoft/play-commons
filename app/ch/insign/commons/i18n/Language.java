package ch.insign.commons.i18n;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import play.i18n.Lang;
import play.mvc.Controller;
import play.mvc.Http;

import java.lang.Exception;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Timo Schmid <timo.schmid@gmail.com>
 */
public class Language  {
	private final static Logger logger = LoggerFactory.getLogger(Language.class);

    private static String forcedLang = null;

    /**
     * Returns the default language from the configuration.
     */
    public static String getDefaultLanguage() {
        return play.Play.application().configuration().getString("cms.defaultLanguage", "en");
    }

    /**
     * @return The Language (2 letter abbr) which the user is currently using. If called from a System-Context (Akka), te DefaultCMSLanguage
     */
    public static String getCurrentLanguage() {
        // Workaround if no Http-Context is available (e.g. if Method is called from Unit-Test)
        if (forcedLang!=null) return forcedLang;

        try {
            Http.Context.current();
        } catch(RuntimeException e) {
            // Fake the current language
            return getDefaultLanguage();
        }

        try {
            return Controller.lang().code();
        } catch(Exception e) {
            return getDefaultLanguage();
        }
    }

    /**
     * @return The Lang (play.i18n.Lang) which the user is currently using. If called from a System-Context (Akka), te DefaultCMSLanguage
     */
    public static Lang getCurrentLang() {
        return new Lang(Lang.apply(getCurrentLanguage()));
    }

    /**
     * Get a list of all language codes configured in the project.
     * (Uses Lang.availables() as base input).
     *
     * @return
     */
    public static List<String> getAllLanguages() {
        List<String> ret = new ArrayList<>();
        for (Lang lang : Lang.availables()) {
               ret.add(lang.code());
        }
        return ret;
    }

    /**
     * Force-set a current language.
     *
     * Warning: This is meant for tests only (as there is no http context available in tests).
     * Do not use this in your application.
     *
     * @param lang language abbrev - or null to switch back to normal language handling
     */
    public static void forceCurrentLanguage(String lang) {
        forcedLang = lang;
    }

}
