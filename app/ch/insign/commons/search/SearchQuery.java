package ch.insign.commons.search;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import ch.insign.commons.i18n.Language;

/**
 * A basic query data object, suitable for default queries.
 * Extend it for specialized searches.
 *
 * Note: The default result limit is set to 100
 */
public class SearchQuery  {
	private final static Logger logger = LoggerFactory.getLogger(SearchQuery.class);

    public String query;
    public int limit = 100;
    public int page;
    public String language = Language.getCurrentLanguage();

    public SearchQuery(String query) {
        this.query = query;

    }

    public SearchQuery(String query, int limit, int page, String language) {
        this.query = query;
        this.limit = limit;
        this.page = page;
        this.language = language;
    }

    public String toString() {
        return String.format("SearchQuery(query: '%s')", query);
    }
}
