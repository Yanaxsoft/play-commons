package ch.insign.commons.search;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;


import java.util.ArrayList;
import java.util.List;

/**
 * Manages the unified searching across multiple search providers.
 * @author bachi
 *
 */
public class SearchManager  {
	private final static Logger logger = LoggerFactory.getLogger(SearchManager.class);

    protected List<SearchProvider> searchProviders = new ArrayList<>();

    /**
     * Register new search provider(s).
     */
    public void register(SearchProvider... providers) {

        for (SearchProvider provider : providers) {
            searchProviders.add(provider);
            provider.attach();
            logger.debug("Added search provider: " + provider);
        }
    }

    /**
     * Detach all attached search providers
     */
    public void flush() {
        for (SearchProvider provider : searchProviders) {
            provider.detach();
        }
        searchProviders.clear();
        logger.debug("Removed search provider(s)");
    }

    public List<SearchProvider> getSearchProviders() {
        return searchProviders;
    }


    /**
     * Get the (first) search provider of the given class
     * @return search provider or null of not found
     */
    public <T extends SearchProvider> T getSearchProviderOf(Class<T> clazz) {
        for (SearchProvider p : searchProviders) {
            if (p.getClass().equals(clazz)) return (T) p;
        }
        return null;
    }

    /**
     * Execute a search
     */
    public List<SearchResult> search (SearchQuery query) {
        List<SearchResult> result = new ArrayList<>();

        if (searchProviders.size()==0) {
            logger.error(String.format("SearchManager: Search for '%s' requested but no SearchProvider is configured!", query.query));
            return result;
        }

        for (SearchProvider provider : searchProviders) {
            // Providers will add their results or modify the result list at will
            provider.search(query, result);
        }

        logger.debug(String.format("Searching for '%s' returned %d results.", query, result.size()));
        return result;
    }

}
