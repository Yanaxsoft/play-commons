package ch.insign.commons.search;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

/**
 * Search results interface
 */
public interface SearchResult {
    public String getTitle();
    public String getSnippet();
    public String getUrl();
    public String getImage();
    public String getAuthor();
    public String getLanguage();
}
