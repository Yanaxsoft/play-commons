package ch.insign.commons.search;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

/**
 * A default implementation of the SearchResult interface
 */
public class DefaultSearchResult implements SearchResult  {
	private final static Logger logger = LoggerFactory.getLogger(DefaultSearchResult.class);

    private String title;
    private String snippet;
    private String url;

    private String language;
    private String image;
    private String author;

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getSnippet() {
        return snippet;
    }

    @Override
    public String getUrl() {
        return url;
    }

    public DefaultSearchResult setTitle(String title) {
        this.title = title;
        return this;
    }

    public DefaultSearchResult setUrl(String url) {
        this.url = url;
        return this;
    }

    public DefaultSearchResult setSnippet(String snippet) {
        this.snippet = snippet;
        return this;
    }

    @Override
    public String getLanguage() {
        return language;
    }

    public DefaultSearchResult setLanguage(String language) {
        this.language = language;
        return this;
    }

    @Override
    public String getImage() {
        return image;
    }

    public DefaultSearchResult setImage(String image) {
        this.image = image;
        return this;
    }

    @Override
    public String getAuthor() {
        return author;
    }

    public DefaultSearchResult setAuthor(String author) {
        this.author = author;
        return this;
    }
}
