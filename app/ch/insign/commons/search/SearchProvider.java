package ch.insign.commons.search;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import java.util.List;

/**
 * Interface for search providers.
 */
public interface SearchProvider {

    /**
     * Execute a search and return the results.
     * The provider will get a list of already added results and can then add his own or
     * modify the list as desired (e.g. replacing generic by more specific results)
     *
     * @param query
     * @param results the list of already found results (used e.g. to calculate paging / limits)
     * @return the updated list of results
     */
    public List<SearchResult> search(SearchQuery query, List<SearchResult> results);

    /**
     * The search provider was attached to the search manager
     */
    public void attach();

    /**
     * The search provider was removed from the search manager
     */
    public void detach();

    /**
     * Add or update index
     * @param id the page's id
     */
    public void updateIndex (String id);

    /**
     * Remove from Index
     * @param id the page's id
     */
    public void removeFromIndex (String id);

    /**
     * Empty and re-fill the search index
     */
    public void rebuildIndex();

}
