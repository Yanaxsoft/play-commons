name := "play-commons"

organization := "ch.insign"

version := "1.3.7"

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  javaCore,
  javaJdbc,
  javaJpa,
  javaWs,
  cache,
  "org.eclipse.persistence" % "eclipselink" % "2.6.2",
  "uaihebert.com" % "EasyCriteria" % "3.0.0",
  "org.jsoup" % "jsoup" % "1.7.3",
  "org.apache.httpcomponents" % "httpcore" % "4.4.4",
  "org.apache.httpcomponents" % "httpclient" % "4.5.1"
)

routesGenerator := StaticRoutesGenerator

// Change max-filename-length for scala-Compiler. Longer filenames (not sure what the threshold is) causes problems
// with encrypted home directories under ubuntu
scalacOptions ++= Seq("-Xmax-classfile-name", "100")

licenses += ("Apache-2.0", url("https://www.apache.org/licenses/LICENSE-2.0.html"))
bintrayRepository := "play-cms"
bintrayOrganization := Some("insign")
publishMavenStyle := true

lazy val commons = (project in file("."))
  .enablePlugins(PlayJava)
